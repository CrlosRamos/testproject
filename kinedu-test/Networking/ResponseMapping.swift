//
//  ResponseMapping.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 04/05/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import Foundation

class ResponseMapping {
    
    func serializeResponse(withData responseData: String) -> [String:Any]? {
        if responseData.data(using: .utf8) != nil {
            do {
                let data = responseData.data(using: .utf8)
                let jsonData = try JSONSerialization.jsonObject(with: data!, options: []) as? [String:Any]
                return jsonData
            } catch {
                print(error.localizedDescription)
                return nil
            }
        } else {
            return nil
        }
    }
}
