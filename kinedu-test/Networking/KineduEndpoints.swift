//
//  KineduEndpoints.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 04/05/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import Foundation

import Foundation
import Moya

enum Kinedu {
    case getNPSScores
}

extension Kinedu: TargetType {
    
    var baseURL: URL { return URL(string: "http://demo.kinedu.com/")! }
    
    var path: String {
        switch self {
        case .getNPSScores:
            return "bi/nps"
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .getNPSScores:
            return ["Content-Type":"application/json"]
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getNPSScores:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getNPSScores:
            return .requestPlain
        }
    }
}
