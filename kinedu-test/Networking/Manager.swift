//
//  Manager.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 04/05/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import Foundation
import Moya

class Manager {
    
    private let provider = MoyaProvider<Kinedu>()
    
    func getNPSSCores(completion: @escaping ([NPSSCore]?, Error?) -> ()) {
        provider.request(.getNPSScores) { result in
            switch result {
            case .success(let response):
              let decoder = JSONDecoder()
              do {
                  let scores = try decoder.decode([NPSSCore].self, from: response.data)
                  completion(scores, nil)
              } catch let error {
                  completion(nil, error)
              }
            case .failure(let error):
                print("Error request: \(error.errorDescription ?? "")")
                completion(nil, error)
            }
        }
    }
}

