//
//  ScoreCard.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 04/05/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import UIKit

class ScoreCard: UIView {
    
    enum UsersType: String {
        case Fremium
        case Premium
    }
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Freemium users"
        label.font = UIFont(name: "GothamRounded-Medium", size: 14)
        label.textColor = UIColor(named: "KineduBlue")
        return label
    }()
    
    private let scoreNumber: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "40"
        label.font = UIFont(name: "GothamRounded-Bold", size: 80)
        label.textColor = UIColor(named: "kineduRed")
        return label
    }()
    
    private let detailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Out of 20 users"
        label.font = UIFont(name: "GothamRounded-Book", size: 12)
        label.textColor = UIColor(named: "grayTitle")
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView(){
        translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(titleLabel)
        addSubview(scoreNumber)
        addSubview(detailLabel)
        
        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        scoreNumber.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 11).isActive = true
        scoreNumber.centerXAnchor.constraint(equalTo: titleLabel.centerXAnchor).isActive = true
        
        detailLabel.topAnchor.constraint(equalTo: scoreNumber.bottomAnchor, constant: 0).isActive = true
        detailLabel.centerXAnchor.constraint(equalTo: scoreNumber.centerXAnchor).isActive = true
    }
    
    func bindView(type:UsersType, score:Int, usersCount: Int){

        if score < 70 {
            scoreNumber.textColor = UIColor(named: "kineduRed")
        }else {
            scoreNumber.textColor = UIColor(named: "kineduGreen")
        }
        
        titleLabel.text = "\(type.rawValue) users"
        scoreNumber.text = "\(score)"
        detailLabel.text = "Out if \(usersCount) users"
    }
    
}
