//
//  UIColor+Kinedu.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 04/05/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import UIKit

extension UIColor {
    
    //#1264A4
    public class var headerBackground: UIColor {
        return UIColor(red: 18/255, green: 100/255, blue: 164/255, alpha: 1)
    }
    
    //#1FADDF
    public class var aquaBlue: UIColor {
        return UIColor(red: 31/255, green: 173/255, blue: 223/255, alpha: 1)
    }
}
