//
//  AppDelegate.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 29/04/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

}

