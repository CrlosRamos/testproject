//
//  DetailVC.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 29/04/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    var detailData:[NPSSCore]!
    var isLoading: Bool = false
    var visibleItemIndex:IndexPath = IndexPath(item: 0, section: 0)
    var previousCell:ScoreCell!
    var versionNumber:String!
    var detailfiltered:[NPSSCore]!
    
    private let headerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .headerBackground
        return view
    }()
    
    private let headerTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Choose a number"
        label.font = UIFont(name: "GothamRounded-Bold", size: 16)
        label.textColor = .white
        return label
    }()
    
    private let layout = ZoomAndSnapFlowLayout()
    
    private lazy var scoreCollection: UICollectionView  = {
        let frame = CGRect(x: 0, y: 0, width:0, height: 0)
        
        let collection = UICollectionView(frame: frame, collectionViewLayout: layout)
        collection.backgroundColor = .clear
        collection.showsHorizontalScrollIndicator = false
        
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    private let cardView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 6
        view.dropShadow()
        return view
    }()
    
    private let cardTitle:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "NPS DETAILS"
        label.font = UIFont(name: "GothamRounded-Book", size: 22)
        label.textColor = UIColor(named: "grayTitle")
        return label
    }()
    
    
    let usersCount: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "GothamRounded-Bold", size: 36)
        label.textColor = .aquaBlue
        label.text = "0"
        return label
    }()
    
    let FreemiumDescription: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "GothamRounded-Book", size: 16)
        label.textColor = .headerBackground
        label.text = "Freemium \nusers"
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private let freemiumUsers: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let usersContainer: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 20
        stack.alignment = .center
        stack.distribution = .fillEqually
        return stack
    }()
    
    
    let premiumUsersCountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "GothamRounded-Bold", size: 36)
        label.textColor = .aquaBlue
        label.text = "0"
        return label
    }()
    
    let premiumDescription: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "GothamRounded-Book", size: 16)
        label.textColor = .headerBackground
        label.text = "Premium \nusers"
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    private let premiumUsers: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    private let footerCard: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .headerBackground
        view.layer.cornerRadius = 6
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        return view
    }()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: "GothamRounded-Medium", size: 14)
        label.textColor = .white
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        filterData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupView(){
        view.backgroundColor = UIColor(named: "kineduBackground")
        title = "NPS Detail \(versionNumber ?? "")"
        
        view.addSubview(headerView)
        view.addSubview(cardView)
        
        headerView.addSubview(headerTitle)
        headerView.addSubview(scoreCollection)
        scoreCollection.dataSource = self
        scoreCollection.delegate = self
        scoreCollection.register(ScoreCell.self, forCellWithReuseIdentifier: "scoreCell")
        
        cardView.addSubview(cardTitle)
        cardView.addSubview(footerCard)
        cardView.addSubview(usersContainer)
        
        usersContainer.addArrangedSubview(freemiumUsers)
        usersContainer.addArrangedSubview(premiumUsers)
        
        freemiumUsers.addSubview(usersCount)
        freemiumUsers.addSubview(FreemiumDescription)
        
        premiumUsers.addSubview(premiumUsersCountLabel)
        premiumUsers.addSubview(premiumDescription)
        
        footerCard.addSubview(messageLabel)
        
        usersCount.topAnchor.constraint(equalTo: freemiumUsers.topAnchor, constant: 0).isActive = true
        usersCount.centerXAnchor.constraint(equalTo: freemiumUsers.centerXAnchor).isActive = true
        
        FreemiumDescription.topAnchor.constraint(equalTo: usersCount.bottomAnchor, constant: 8).isActive = true
        FreemiumDescription.centerXAnchor.constraint(equalTo: usersCount.centerXAnchor).isActive = true
        FreemiumDescription.bottomAnchor.constraint(equalTo: freemiumUsers.bottomAnchor, constant: 0).isActive = true
        
        premiumUsersCountLabel.topAnchor.constraint(equalTo: premiumUsers.topAnchor, constant: 0).isActive = true
        premiumUsersCountLabel.centerXAnchor.constraint(equalTo: premiumUsers.centerXAnchor).isActive = true
        
        premiumDescription.topAnchor.constraint(equalTo: premiumUsersCountLabel.bottomAnchor, constant: 8).isActive = true
        premiumDescription.centerXAnchor.constraint(equalTo: premiumUsersCountLabel.centerXAnchor).isActive = true
        premiumDescription.bottomAnchor.constraint(equalTo: premiumUsers.bottomAnchor, constant: 0).isActive = true
        
        headerView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        headerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.42).isActive = true
        
        headerTitle.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 25).isActive = true
        headerTitle.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
        
        scoreCollection.anchor(top: headerTitle.bottomAnchor, left: headerView.leftAnchor, bottom: headerView.bottomAnchor, right: headerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
        cardView.anchor(top: headerView.bottomAnchor, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, paddingTop: 20, paddingLeft: 12, paddingBottom: 49, paddingRight: 12)
        
        cardTitle.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 47).isActive = true
        cardTitle.centerXAnchor.constraint(equalTo: cardView.centerXAnchor).isActive = true
        
        usersContainer.anchor(top: cardTitle.bottomAnchor, left: cardView.leftAnchor, bottom: footerCard.topAnchor, right: cardView.rightAnchor, paddingTop: 32, paddingLeft: 50, paddingBottom: 28, paddingRight: 50)
        
        footerCard.anchor(top: nil, left: cardView.leftAnchor, bottom: cardView.bottomAnchor, right: cardView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        footerCard.heightAnchor.constraint(equalTo: cardView.heightAnchor, multiplier: 0.26).isActive = true

        
        messageLabel.anchor(top: footerCard.topAnchor, left: footerCard.leftAnchor, bottom: footerCard.bottomAnchor, right: footerCard.rightAnchor, paddingTop: 14, paddingLeft: 37, paddingBottom: 14, paddingRight: 37)
    }
    
    func bindCardView(freemiumUsersCount:Int, premiumUsersCount:Int){
        usersCount.text = "\(freemiumUsersCount)"
        premiumUsersCountLabel.text = "\(premiumUsersCount)"
        let element = Dictionary(grouping: detailfiltered , by: { $0.activity_views })
        let highestValue = element.sorted(by: { $0.value.count > $1.value.count }).first?.value
        
        let usersWithValue = highestValue?.count ?? 0
        
        let percentage = calculatePercentage(value: Double(usersWithValue), totalValues: Double(premiumUsersCount + freemiumUsersCount))
        
        let attrs1 = [NSAttributedString.Key.font : UIFont(name: "GothamRounded-Bold",size: 18), NSAttributedString.Key.foregroundColor : UIColor(named: "kineduGreen")]
        let attrs2 = [NSAttributedString.Key.font : UIFont(name: "GothamRounded-Medium",size: 14), NSAttributedString.Key.foregroundColor : UIColor.white]
        let attrs3 = [NSAttributedString.Key.font :UIFont(name: "GothamRounded-Bold",size: 18), NSAttributedString.Key.foregroundColor : UIColor.aquaBlue]
        
        let attributedString1 = NSMutableAttributedString(string:"\(Int(percentage))% ", attributes:attrs1 as [NSAttributedString.Key : Any])
        let attributedString2 = NSMutableAttributedString(string:"of the users that answered \(visibleItemIndex.row) their NPS score saw ", attributes:attrs2 as [NSAttributedString.Key : Any])
        let attributedString3 = NSMutableAttributedString(string:"\(highestValue?.first?.activity_views ?? 0) activities", attributes:attrs3 as [NSAttributedString.Key : Any])
        
        attributedString1.append(attributedString2)
        attributedString1.append(attributedString3)
        
        messageLabel.attributedText = attributedString1
    }
    
    func filterData(){
        detailfiltered = detailData.filter{ $0.nps == visibleItemIndex.row }
        
        let freemiumCount = detailfiltered.filter{ $0.user_plan == "freemium" }.count
        let premiumCount = detailfiltered.count - freemiumCount
        
        bindCardView(freemiumUsersCount: freemiumCount, premiumUsersCount: premiumCount)
    }
    
    func calculatePercentage(value:Double,totalValues:Double)->Double{
        print(value, totalValues)
        return value/totalValues * 100
    }

}


extension DetailVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "scoreCell", for: indexPath) as? ScoreCell else { return UICollectionViewCell() }
        cell.bindCell(scoreNumber: indexPath.row)
        
        if previousCell == nil {
            previousCell = cell
            cell.showBorder()
        }
        
        if indexPath.row != visibleItemIndex.row {
            cell.hideBorder()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        scoreCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        let currentCell = scoreCollection.cellForItem(at: indexPath) as! ScoreCell
        previousCell.hideBorder()
        previousCell = currentCell
        visibleItemIndex = indexPath
        currentCell.showBorder()
        filterData()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = scoreCollection.contentOffset
        visibleRect.size = scoreCollection.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        visibleItemIndex = scoreCollection.indexPathForItem(at: visiblePoint)!
        previousCell.hideBorder()
        let currentCell = scoreCollection.cellForItem(at: visibleItemIndex) as! ScoreCell
        previousCell = currentCell
        currentCell.showBorder()
        
        //filter data
        filterData()
    }
}
