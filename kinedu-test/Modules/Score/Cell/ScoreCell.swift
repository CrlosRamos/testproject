//
//  ScoreCell.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 04/05/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import UIKit

class ScoreCell: UICollectionViewCell {
    
    private let icon: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "baby_0")
        return image
    }()
    
    private let border: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.isHidden = true
        return view
    }()
    
    private let score: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font =  UIFont(name: "GothamRounded-Medium", size: 22)
        label.text = "0"
        label.textColor = .white
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        border.layer.cornerRadius = border.frame.width / 2
        border.layer.masksToBounds = true
    }
    
    private func setupView(){
        addSubview(border)
        addSubview(icon)
        addSubview(score)
        
        icon.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10)
        border.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 6, paddingLeft: 6, paddingBottom: 6, paddingRight: 6)
        
        score.topAnchor.constraint(equalTo: icon.bottomAnchor, constant: 8).isActive = true
        score.centerXAnchor.constraint(equalTo: icon.centerXAnchor).isActive = true
    }
    
    func showBorder(){
        border.hideWithAnimation(hidden: false)
    }
    
    func hideBorder(){
        border.hideWithAnimation(hidden: true)
    }
    
    func bindCell(scoreNumber:Int){
        icon.image = UIImage(named: "baby_\(scoreNumber)")
        score.text = "\(scoreNumber)"
    }
}
