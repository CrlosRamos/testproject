//
//  ScoreVC.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 29/04/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import UIKit

class ScoreVC: UIViewController {
    
    let manager = Manager()
    var versionNumber = [String]()
    var groupedScores = Dictionary<String,Any>()
    var versionSelected = ""
    
    let titleLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Chose app version"
        label.font = UIFont(name: "GothamRounded-Bold", size: 16)
        label.textColor = UIColor(named: "grayTitle")
        return label
    }()
    
    let segmentedControl: UISegmentedControl = {
        let items = ["v1", "v2", "v3"]
        let segment = UISegmentedControl(items: items)
        segment.translatesAutoresizingMaskIntoConstraints = false
        segment.selectedSegmentIndex = 1
        segment.addTarget(self, action: #selector(selectionDidChange), for: .valueChanged)
        segment.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        segment.setTitleTextAttributes([.foregroundColor: UIColor(named: "KineduBlue")!, .font: UIFont(name: "GothamRounded-Bold", size: 12)!], for: .normal)
        segment.selectedSegmentTintColor = UIColor(named: "kineduOrange")
        return segment
    }()
    
    let cardView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 6
        view.dropShadow()
        return view
    }()
    
    let scoreCard: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let cardTitle:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "NPS SCORE"
        label.font = UIFont(name: "GothamRounded-Book", size: 22)
        label.textColor = UIColor(named: "grayTitle")
        return label
    }()
    
    let fremiumScore: ScoreCard = {
        let view = ScoreCard()
        view.bindView(type: .Fremium, score: 40, usersCount: 20)
        return view
    }()
    
    let premiumScore: ScoreCard = {
        let view = ScoreCard()
        view.bindView(type: .Premium, score: 73, usersCount: 159)
        return view
    }()
    
    let separator:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
        return view
    }()
    
    let detailButton:UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("See more details", for: .normal)
        button.backgroundColor = UIColor(named: "kineduGreen")
        button.titleLabel?.font = UIFont(name: "GothamRounded-Bold", size: 16)
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func showLoadingAlert(){
        let alert = UIAlertController(title: nil, message: "Please wait...😁", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating()
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func getData(){
        
        showLoadingAlert()
        manager.getNPSSCores { result, error in
            if let error = error {
                print(error)
                self.dismiss(animated: false, completion: nil)
                return
            }
            self.groupedScores = Dictionary(grouping: result! , by: { $0.build.version})
            self.versionNumber = self.groupedScores.keys.sorted()
            
            var index = 0
            for version in self.versionNumber {
                self.segmentedControl.setTitle(version, forSegmentAt: index)
                index += 1
            }
            self.versionSelected = self.versionNumber.first!
            self.updateView()
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    private func setupView(){
        view.backgroundColor = UIColor(named: "kineduBackground")
        title = "Kinedu NPS Score"

        view.addSubview(titleLabel)
        view.addSubview(segmentedControl)
        view.addSubview(cardView)
        view.addSubview(detailButton)
        
        cardView.addSubview(cardTitle)
        cardView.addSubview(fremiumScore)
        cardView.addSubview(premiumScore)
        cardView.addSubview(separator)

        titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 25).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        segmentedControl.anchor(top: titleLabel.bottomAnchor, left: view.safeAreaLayoutGuide.leftAnchor, bottom: nil, right: view.safeAreaLayoutGuide.rightAnchor, paddingTop: 17, paddingLeft: 16, paddingBottom: 0, paddingRight: 16)
        
        cardView.anchor(top: segmentedControl.bottomAnchor, left: segmentedControl.leftAnchor, bottom: nil, right: segmentedControl.rightAnchor, paddingTop: 30, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
        cardTitle.topAnchor.constraint(equalTo: cardView.topAnchor, constant: 27.5).isActive = true
        cardTitle.centerXAnchor.constraint(equalTo: cardView.centerXAnchor).isActive = true
        
        fremiumScore.anchor(top: cardTitle.bottomAnchor, left: cardView.leftAnchor, bottom: nil, right: cardView.rightAnchor, paddingTop: 7.5, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        
        premiumScore.anchor(top: fremiumScore.bottomAnchor, left: cardView.leftAnchor, bottom: cardView.bottomAnchor, right: cardView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        premiumScore.heightAnchor.constraint(equalTo: cardView.heightAnchor, multiplier: 0.4).isActive = true
        
        separator.anchor(top: premiumScore.topAnchor, left: cardView.leftAnchor, bottom: nil, right: cardView.rightAnchor, paddingTop: -22, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
        separator.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        detailButton.anchor(top: cardView.bottomAnchor, left: cardView.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: cardView.rightAnchor, paddingTop: 29, paddingLeft: 34, paddingBottom: 29, paddingRight: 34)
        detailButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        detailButton.addTarget(self, action: #selector(showDetail), for: .touchUpInside)
        segmentedControl.selectedSegmentIndex = 0
    }
    
    func average(numbers: Int...) -> Double {
           var sum = 0
           for number in numbers {
               sum += number
           }
           let  ave : Double = Double(sum) / Double(numbers.count)
           return ave
    }
    
    private func updateView() {
        let scoresByVersion = groupedScores[versionSelected] as? [NPSSCore]
        let freemiumUsers = scoresByVersion?.filter{ $0.user_plan == "freemium" }
        let premiumUsers = scoresByVersion?.filter{ $0.user_plan == "premium" }
        
        let free = Dictionary(grouping: freemiumUsers! , by: { $0.days_since_signup })
        let freeValue = free.sorted(by: { $0.value.count > $1.value.count }).first?.value
        
        let pro = Dictionary(grouping: premiumUsers! , by: { $0.days_since_signup })
        let proValue = pro.sorted(by: { $0.value.count > $1.value.count }).first?.value
        
        fremiumScore.bindView(type: .Fremium, score: freeValue?.count ?? 0, usersCount: freemiumUsers?.count ?? 0)
        premiumScore.bindView(type: .Premium, score: proValue?.count ?? 0, usersCount: premiumUsers?.count ?? 0)
        
    }
    
    @objc func showDetail(){
        let vc = DetailVC()
        vc.detailData = groupedScores[versionSelected] as? [NPSSCore]
        vc.versionNumber = versionSelected
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func selectionDidChange(_ sender: UISegmentedControl) {
        versionSelected = versionNumber[sender.selectedSegmentIndex]
        updateView()
    }
    
}
