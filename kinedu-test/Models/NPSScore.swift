//
//  NPSScore.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 04/05/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import Foundation

struct NPSSCore:Decodable {
    let nps: Int
    let days_since_signup: Int
    let user_plan: String
    let activity_views: Int
    let build: Build
    
    private enum CodingKeys: String, CodingKey { case nps,days_since_signup, user_plan,activity_views, build }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        nps = try container.decode(Int.self, forKey: .nps)
        days_since_signup = try container.decode(Int.self, forKey: .days_since_signup)
        user_plan = try container.decode(String.self, forKey: .user_plan)
        activity_views = try container.decode(Int.self, forKey: .activity_views)
        build = try container.decode(Build.self, forKey: .build)
    }
}

struct Build:Decodable {
    let version: String
    let release_date: String
}
